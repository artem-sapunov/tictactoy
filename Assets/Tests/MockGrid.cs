using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;

namespace Tests
{
    public class MockGrid : IGrid
    {
        public event Action<Vector2Int> OnCellClicked;
        
        public int Height { get; private set; }
        public int Width { get; private set; }

        private ICell[,] _cells;
        private int _filledCellsCounter;
        
        public UniTask Init(int height, int width)
        {
            Height = height;
            Width = width;
            _cells = new ICell[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    ICell cell = new MockCell();
                    _cells[x, y] = cell;
                    cell.OnClick += OnCellClickedHandler;
                    cell.OnMarkChanged += OnMarkChangedHandler;
                }
            }
            
            return UniTask.CompletedTask;
        }
        
        public void Dispose()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    ICell cell = _cells[x, y];
                    cell.OnClick -= OnCellClickedHandler;
                    cell.OnMarkChanged -= OnMarkChangedHandler;
                }
            }

            _cells = null;
        }

        public ICell GetCell(Vector2Int position)
        {
            if (position.x < 0 || position.x > Width - 1 || position.y < 0 || position.y > Height - 1)
            {
                return null;
            }
            
            return _cells[position.x, position.y];
        }

        public bool HasNoEmptySpace()
        {
            return _filledCellsCounter == Width * Height;
        }

        public void ClearField()
        {
            foreach (ICell cell in _cells)
            {
                cell.SetMark(SymbolType.None);
            }
        }

        public Vector2 CalculateGridSize()
        {
            throw new NotImplementedException();
        }

        public Vector3 GetWorldPosition(Vector2Int position)
        {
            throw new NotImplementedException();
        }

        public Vector3 GetCenterPosition()
        {
            throw new NotImplementedException();
        }
        
        private void OnCellClickedHandler(ICell cell)
        {
            OnCellClicked?.Invoke(cell.Position);
        }
        
        private void OnMarkChangedHandler(ICell cell, SymbolType prevType, SymbolType newType)
        {
            if (newType == SymbolType.None)
            {
                _filledCellsCounter--;
            }
            else if(prevType == SymbolType.None)
            {
                _filledCellsCounter++;
            }
        }
    }
}