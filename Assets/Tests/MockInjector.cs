using UnityEngine;
using Zenject;

namespace Tests
{
    public class MockInjector : IInjector
    {
        private DiContainer _diContainer;
        
        public MockInjector(DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        public DiContainer CurrentContainer => _diContainer;

        public void Inject(GameObject gameObject)
        {
            _diContainer.InjectGameObject(gameObject);
        }

        public void Inject(object obj)
        {
            _diContainer.Inject(obj);
        }
    }
}