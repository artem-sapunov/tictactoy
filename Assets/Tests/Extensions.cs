using System;
using System.Collections;
using Cysharp.Threading.Tasks;

namespace Tests
{
    public static class Extensions
    {
        public static IEnumerator AsIEnumeratorReturnNull<T>(this UniTask<T> task)
        {
            while (!task.Status.IsCompleted())
            {
                yield return null;
            }

            if (task.Status.IsFaulted())
            {
                throw new Exception();
            }

            yield return null;
        }
        
        public static IEnumerator AsIEnumeratorReturnNull(this UniTask task)
        {
            while (!task.Status.IsCompleted())
            {
                yield return null;
            }

            if (task.Status.IsFaulted())
            {
                throw new Exception();
            }

            yield return null;
        }
    }
}