using System.Collections;
using Gameplay.Systems.Hint;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace Tests
{
    [TestFixture]
    public class HintTest : ZenjectUnitTestFixture
    {
        private IGrid _grid;
        private ITicTacToeController _controller;
        private IHintSystem _hintSystem;
        private MockPlayer _player1;
        private MockPlayer _player2;
        private bool _nextTurn;
        private bool _hintAvailable;
        private HintData _hintData;
        private WaitUntil _waitTurnInstruction;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            
            _waitTurnInstruction = new WaitUntil(() =>
            {
                if (_nextTurn)
                {
                    _nextTurn = false;
                    return true;
                }

                return false;
            });
            
            Container.Bind<IGrid>().To<MockGrid>().AsSingle();
            Container.Bind<IMatchChecker>().To<MatchChecker>().AsSingle();
            Container.Bind<ITicTacToeController>().To<TicTacToeController>().AsSingle();
            Container.Bind<IHintSystem>().To<HintSystem>().AsSingle();
            
            _controller = Container.Resolve<ITicTacToeController>();
            _grid = Container.Resolve<IGrid>();
            _hintSystem = Container.Resolve<IHintSystem>();
            
            _player1 = new MockPlayer("", SymbolType.X);
            _player2 = new MockPlayer("", SymbolType.O);
            
            _player1.OnTurn += OnPlayerTurnHandler;
            _player2.OnTurn += OnPlayerTurnHandler;
            
            _controller.Init(_player1, _player2);

            _hintSystem.Init(new [] { SymbolType.X });
            _hintSystem.HintChanged += OnHintChangedHandler;
        }

        [TearDown]
        public override void Teardown()
        {
            base.Teardown();
            
            _player1.OnTurn -= OnPlayerTurnHandler;
            _player2.OnTurn -= OnPlayerTurnHandler;
            
            _hintSystem.HintChanged -= OnHintChangedHandler;

            _nextTurn = false;
            _controller.Dispose();
        }
        
        [UnityTest]
        public IEnumerator Hint()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player2.SymbolType);
            Assert.IsTrue(_hintData == null);
            
            _player2.SetNextMove(new Vector2Int(1, 0));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player1.SymbolType);
            Assert.IsTrue(CheckHint(new Vector2Int(0, 1)));
            
            _player1.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player2.SymbolType);
            Assert.IsTrue(_hintData == null);
            
            _player2.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player1.SymbolType);
            Assert.IsTrue(CheckHint(new Vector2Int(0, 2)));
            
            _player1.SetNextMove(new Vector2Int(1, 2)); 
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player2.SymbolType);
            Assert.IsTrue(_hintData == null);
            
            _player2.SetNextMove(new Vector2Int(0, 2));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player1.SymbolType);
            Assert.IsTrue(CheckHint(new Vector2Int(2, 2)));
            
            _player1.SetNextMove(new Vector2Int(2, 0));
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player2.SymbolType);
            Assert.IsTrue(_hintData == null);
            
            _player2.SetNextMove(new Vector2Int(2, 1)); 
            yield return _waitTurnInstruction;
            
            _hintSystem.ShowHint(_player1.SymbolType);
            Assert.IsTrue(CheckHint(new Vector2Int(2, 2)));
            
            _player1.SetNextMove(new Vector2Int(2, 2));
            
        }

        private bool CheckHint(Vector2Int position)
        {
            if (_hintData != null)
            {
                bool isPositionEquals = _hintData.TargetPosition == position;
                _hintData = null;
                return isPositionEquals;
            }

            _hintData = null;

            return false;
        }

        private void OnPlayerTurnHandler()
        {
            _nextTurn = true;
        }
        
        private void OnHintChangedHandler(HintData hintData)
        {
            _hintAvailable = true;
            _hintData = hintData;
        }
    }
}