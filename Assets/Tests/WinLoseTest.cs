using System.Collections;
using AssetManagement;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using Gameplay.TicTacToe.Players;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace Tests
{
    [TestFixture]
    public class WinLoseTest : ZenjectUnitTestFixture
    {
        private IGrid _grid;
        private ITicTacToeController _controller;
        private MockPlayer _player1;
        private MockPlayer _player2;
        private bool _nextTurn;
        private bool _draw;
        private SymbolType _winSymbolType;
        private WaitUntil _waitTurnInstruction;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            
            _waitTurnInstruction = new WaitUntil(() =>
            {
                if (_nextTurn)
                {
                    _nextTurn = false;
                    return true;
                }

                return false;
            });
            
            Container.Bind<IGrid>().To<MockGrid>().AsSingle();
            Container.Bind<IMatchChecker>().To<MatchChecker>().AsSingle();
            Container.Bind<ITicTacToeController>().To<TicTacToeController>().AsSingle();
            
            _grid = Container.Resolve<IGrid>();
            _controller = Container.Resolve<ITicTacToeController>();

            _player1 = new MockPlayer("", SymbolType.X);
            _player2 = new MockPlayer("", SymbolType.O);
            
            _player1.OnTurn += OnPlayerTurnHandler;
            _player2.OnTurn += OnPlayerTurnHandler;
            
            _controller.Init(_player1, _player2);
        }

        [TearDown]
        public override void Teardown()
        {
            base.Teardown();
            
            _player1.OnTurn -= OnPlayerTurnHandler;
            _player2.OnTurn -= OnPlayerTurnHandler;

            _nextTurn = false;
            _draw = false;
            _winSymbolType = SymbolType.None;
            _controller.Dispose();
        }


        [UnityTest]
        public IEnumerator WinHorizontal()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnWinPlayer += WinPlayerHandler;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 2));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 2));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 2));
            
            Assert.IsTrue(_winSymbolType == _player1.SymbolType);
            
            _controller.OnWinPlayer -= WinPlayerHandler;
        }

        [UnityTest]
        public IEnumerator WinVertical()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnWinPlayer += WinPlayerHandler;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 0));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 2));
            
            Assert.IsTrue(_winSymbolType == _player1.SymbolType);
            
            _controller.OnWinPlayer -= WinPlayerHandler;
        }
        
        [UnityTest]
        public IEnumerator WinDiagonalBLeftTRight()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnWinPlayer += WinPlayerHandler;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 2));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 2));
            
            Assert.IsTrue(_winSymbolType == _player1.SymbolType);
            
            _controller.OnWinPlayer -= WinPlayerHandler;
        }
        
        [UnityTest]
        public IEnumerator WinDiagonalTLeftBRight()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnWinPlayer += WinPlayerHandler;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 2));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 2));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 0));
            
            Assert.IsTrue(_winSymbolType == _player1.SymbolType);
            
            _controller.OnWinPlayer -= WinPlayerHandler;
        }
        
        [UnityTest]
        public IEnumerator Lose()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnWinPlayer += WinPlayerHandler;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 0));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 2));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(2, 0));
            
            Assert.IsTrue(_winSymbolType == _player2.SymbolType);
            
            _controller.OnWinPlayer -= WinPlayerHandler;
        }
        
        [UnityTest]
        public IEnumerator Draw()
        {
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.OnDraw += OnDraw;
            _controller.StartGame();
            
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 0));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 0));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 1));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 1));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(1, 2)); 
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 2));
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 0));
            yield return _waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(2, 1)); 
            yield return _waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 2));
            
            Assert.IsTrue(_draw);
            
            _controller.OnDraw -= OnDraw;
        }

        private void OnPlayerTurnHandler()
        {
            _nextTurn = true;
        }
        
        private void WinPlayerHandler(IPlayer player, IMatch match)
        {
            _winSymbolType = player.SymbolType;
        }

        private void OnDraw()
        {
            _draw = true;
        }
    }
}