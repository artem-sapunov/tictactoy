using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Players;
using UnityEngine;

namespace Tests
{
    public class MockPlayer : IPlayer
    {
        public string Name { get; }
        public SymbolType SymbolType { get; }

        private UniTaskCompletionSource<Vector2Int> _tcs;

        public event Action OnTurn; 

        public MockPlayer(string name, SymbolType symbolType)
        {
            Name = name;
            SymbolType = symbolType;
        }
        
        public async UniTask<Vector2Int> Move(CancellationToken cancellationToken)
        {
            _tcs = new UniTaskCompletionSource<Vector2Int>();
            OnTurn?.Invoke();
            Vector2Int move = await _tcs.Task;
            return move;
        }

        public void SetNextMove(Vector2Int nextMove)
        {
            _tcs.TrySetResult(nextMove);
        }
    }
}