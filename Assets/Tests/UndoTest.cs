﻿using System.Collections;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace Tests
{
    [TestFixture]
    public class UndoTest : ZenjectUnitTestFixture
    {
        private IGrid _grid;
        private ITicTacToeController _controller;
        private MockPlayer _player1;
        private MockPlayer _player2;
        private bool _nextTurn;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            
            Container.Bind<IGrid>().To<MockGrid>().AsSingle();
            Container.Bind<IMatchChecker>().To<MatchChecker>().AsSingle();
            Container.Bind<ITicTacToeController>().To<TicTacToeController>().AsSingle();
            
            _grid = Container.Resolve<IGrid>();
            _controller = Container.Resolve<ITicTacToeController>();

            _player1 = new MockPlayer("", SymbolType.X);
            _player2 = new MockPlayer("", SymbolType.O);

            _player1.OnTurn += OnPlayerTurnHandler;
            _player2.OnTurn += OnPlayerTurnHandler;
            
            _controller.Init(_player1, _player2);
        }

        [TearDown]
        public override void Teardown()
        {
            base.Teardown();

            _player1.OnTurn -= OnPlayerTurnHandler;
            _player2.OnTurn -= OnPlayerTurnHandler;

            _nextTurn = false;
            
            _controller.Dispose();
        }
        
        
        [UnityTest]
        public IEnumerator Undo()
        {
            WaitUntil waitTurnInstruction = new WaitUntil(() =>
            {
                if (_nextTurn)
                {
                    _nextTurn = false;
                    return true;
                }

                return false;
            });
            
            yield return _grid.Init(3, 3).AsIEnumeratorReturnNull();
            
            _controller.StartGame();
            
            yield return waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(0, 2));
            yield return waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(1, 2));
            yield return waitTurnInstruction;
            _player1.SetNextMove(new Vector2Int(2, 2));
            yield return waitTurnInstruction;
            _player2.SetNextMove(new Vector2Int(0, 1));
            yield return waitTurnInstruction;
            
            _controller.Undo();
            
            Assert.IsTrue(_grid.GetCell(new Vector2Int(0, 1)).SymbolType == SymbolType.None);
            Assert.IsTrue(_grid.GetCell(new Vector2Int(2, 2)).SymbolType == SymbolType.None);
        }

        private void OnPlayerTurnHandler()
        {
            _nextTurn = true;
        }
    }
}
