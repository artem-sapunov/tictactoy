using System;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;

namespace Tests
{
    public class MockCell : ICell
    {
        public event Action<ICell> OnClick;
        public event Action<ICell, SymbolType, SymbolType> OnMarkChanged;
        
        public Vector2Int Position { get; private set; }
        public SymbolType SymbolType { get; private set; }
        
        public void SetMark(SymbolType symbolType)
        {
            if (SymbolType == symbolType)
            {
                return;
            }

            SymbolType oldType = SymbolType;
            SymbolType = symbolType;
            
            OnMarkChanged?.Invoke(this, oldType, symbolType);
        }

        public void SetPosition(Vector2Int position)
        {
            Position = position;
        }

        public void SetHint(bool isActive)
        {
            
        }
    }
}