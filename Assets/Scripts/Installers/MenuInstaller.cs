using UI;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class MenuInstaller : MonoInstaller
    {
        [SerializeField] private UIManager _uiManager;

        public override void InstallBindings()
        {
            Container.Bind<UIManager>().FromInstance(_uiManager).AsSingle();
        }
    }
}