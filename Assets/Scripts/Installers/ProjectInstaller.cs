using AssetManagement;
using Audio;
using Factories;
using Gameplay.TicTacToe.AssetsResources;
using States;
using UI;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class ProjectInstaller : MonoInstaller
    {
        [SerializeField] private Curtains _curtainsPrefab;
        [SerializeField] private UIManager _uiManagerPrefab;
        [SerializeField] private AudioSystem _audioSystem;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<Curtains>().FromComponentInNewPrefab(_curtainsPrefab).AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<UIManager>().FromComponentInNewPrefab(_uiManagerPrefab).AsSingle().NonLazy();

            Container.Bind<SceneLoader>().AsSingle();
            Container.Bind<IInjector>().To<Injector>().AsSingle();
            Container.Bind<IAssetsLoader>().To<AssetsLoaderProvider>().AsSingle();
            Container.Bind<AudioSystem>().FromInstance(_audioSystem).AsSingle();
            
            Container.Bind<ITicTacToeInitializerFactory>().To<TicTacToeInitializerFactory>().AsSingle();
            Container.Bind<ITicTacToeResources>().To<TicTacToeResources>().AsSingle();
            
            //Game states
            Container.Bind<IGameStateMachine>().To<GameStateMachine>().AsSingle();

            Container.BindInterfacesAndSelfTo<InitialState>().AsSingle();
            Container.BindInterfacesAndSelfTo<SelectGameModeState>().AsSingle();
            Container.BindInterfacesAndSelfTo<SelectDifficultyState>().AsSingle();
            Container.BindInterfacesAndSelfTo<InitializePlayerVsPlayerState>().AsSingle();
            Container.BindInterfacesAndSelfTo<InitializePlayerVsComputerState>().AsSingle();
            Container.BindInterfacesAndSelfTo<InitializeComputerVsComputerState>().AsSingle();
            Container.BindInterfacesAndSelfTo<InitializeGameplayState>().AsSingle();
            Container.BindInterfacesAndSelfTo<GameLoopState>().AsSingle();
            Container.BindInterfacesAndSelfTo<RestartGameState>().AsSingle();
            Container.BindInterfacesAndSelfTo<ExitGameplayState>().AsSingle();
        }
    }
}