using Gameplay.Systems;
using Gameplay.Systems.GameplayTime;
using Gameplay.Systems.Hint;
using Gameplay.TicTacToe.Background;
using Gameplay.TicTacToe.Camera;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using UI.Gameplay.Hud;
using UI.Gameplay.Undo;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class GamePlayInstaller : MonoInstaller
    {
        [SerializeField] private GridSystem _gridSystem;
        [SerializeField] private CameraController _cameraController;
        [SerializeField] private BackgroundController _backgroundController;
        [SerializeField] private TicTacToeHudPresenter _hudPresenter;
        [SerializeField] private GameplayTimerSystem _gameplayTimerSystem;
        
        public override void InstallBindings()
        {
            Container.Bind<ITicTacToeController>().To<TicTacToeController>().AsSingle();
            Container.Bind<IGrid>().FromInstance(_gridSystem).AsSingle();
            Container.Bind<ICameraController>().FromInstance(_cameraController).AsSingle();
            Container.Bind<IBackgroundController>().FromInstance(_backgroundController).AsSingle();
            Container.Bind<ITicTacToeHudPresenter>().FromInstance(_hudPresenter).AsSingle();
            Container.Bind<IGameplayTime>().FromInstance(_gameplayTimerSystem).AsSingle();
            Container.Bind<IMatchChecker>().To<MatchChecker>().AsSingle();
            Container.Bind<IHintSystem>().To<HintSystem>().AsSingle();
        }
    }
}