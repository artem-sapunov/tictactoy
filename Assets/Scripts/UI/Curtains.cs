﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace UI
{
    public class Curtains : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private float _fadeDuration = 1;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            _canvasGroup.alpha = 1;
        }

        public void Hide()
        {
            if (gameObject.activeInHierarchy)
            {
                FadeOutCoroutine().Forget();
            }
        }

        private async UniTask FadeOutCoroutine()
        {
            var multiplier = 1 / _fadeDuration;

            while (!Mathf.Approximately(_canvasGroup.alpha, 0))
            {
                _canvasGroup.alpha -= Time.deltaTime * multiplier;
                await UniTask.Yield();
            }

            gameObject.SetActive(false);
        }
    }
}