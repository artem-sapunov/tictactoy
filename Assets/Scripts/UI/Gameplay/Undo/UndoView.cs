using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Undo
{
    public class UndoView : MonoBehaviour
    {
        [SerializeField] private Button _button;
        
        public event Action OnUndoClicked;

        private void OnEnable()
        {
            _button.onClick.AddListener(OnClickHandler);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnClickHandler);
        }

        public void SetButtonActive(bool isActive)
        {
            _button.interactable = isActive;
        }
        
        private void OnClickHandler()
        {
            OnUndoClicked?.Invoke();
        }
    }
}