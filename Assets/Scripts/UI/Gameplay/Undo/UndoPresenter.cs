using System.Collections.Generic;
using System.Linq;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Players;
using UnityEngine;
using Zenject;

namespace UI.Gameplay.Undo
{
    public class UndoPresenter : MonoBehaviour
    {
        [Inject] private ITicTacToeController _ticTacToeController;
        
        [SerializeField] private UndoView _undoView;
        
        private IReadOnlyCollection<SymbolType> _allowedPlayers;

        private void Start()
        {
            _undoView.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            _ticTacToeController.OnPlayerTurn += OnPlayerTurn;
            _undoView.OnUndoClicked += OnUndoClickedHandler;
        }

        private void OnDisable()
        {
            _ticTacToeController.OnPlayerTurn -= OnPlayerTurn;
            _undoView.OnUndoClicked -= OnUndoClickedHandler;
        }

        public void Initialize(IReadOnlyCollection<SymbolType> allowedPlayers)
        {
            _allowedPlayers = allowedPlayers;
            _undoView.gameObject.SetActive(true);
        }

        private void OnPlayerTurn(IPlayer player)
        {
            bool isUndoAllowed = _allowedPlayers.Contains(player.SymbolType);
            _undoView.SetButtonActive(isUndoAllowed);
        }

        private void OnUndoClickedHandler()
        {
            _ticTacToeController.Undo();
        }
    }
}