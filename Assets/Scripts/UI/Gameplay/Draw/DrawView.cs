using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Draw
{
    public class DrawView : UIWindow
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _backToMainMenuButton;

        public event Action OnRestartClicked;
        public event Action OnBackToMainMenuClicked;
        
        private void OnEnable()
        {
            _restartButton.onClick.AddListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.AddListener(BackToMainMenuClickedHandler);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.RemoveListener(BackToMainMenuClickedHandler);
        }

        private void RestartClickedHandler()
        {
            OnRestartClicked?.Invoke();
        }

        private void BackToMainMenuClickedHandler()
        {
            OnBackToMainMenuClicked?.Invoke();
        }
    }
}