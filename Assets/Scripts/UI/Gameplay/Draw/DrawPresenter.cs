using States;
using UnityEngine;
using Zenject;

namespace UI.Gameplay.Draw
{
    public class DrawPresenter : MonoBehaviour
    {
        [Inject] private IGameStateMachine _gameStateMachine;
        [Inject] private UIManager _uiManager;
        
        [SerializeField] private DrawView _view;

        private void OnEnable()
        {
            _view.OnRestartClicked += OnRestartClickedHandler;
            _view.OnBackToMainMenuClicked += OnBackToMainMenuClickedHandler;
        }

        private void OnDisable()
        {
            _view.OnRestartClicked -= OnRestartClickedHandler;
            _view.OnBackToMainMenuClicked -= OnBackToMainMenuClickedHandler;
        }
        
        private void OnRestartClickedHandler()
        {
            _uiManager.Hide<DrawView>();
            _gameStateMachine.Enter<RestartGameState>();
        }
        
        private void OnBackToMainMenuClickedHandler()
        {
            _uiManager.Hide<DrawView>();
            _gameStateMachine.Enter<ExitGameplayState>();
        }
    }
}