﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Hud
{
    public class TicTacToeHudView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _passedTimeText;
        [SerializeField] private Button _settingsButton;
        [SerializeField] private Button _hintButton;
        [SerializeField] private Button _changeGraphicButton;
        [SerializeField] private Button _soundButton;
        [SerializeField] private Sprite _activeSoundSprite;
        [SerializeField] private Sprite _inactiveSoundSprite;
        
        public event Action OnSettingsClicked;
        public event Action OnHintClicked;
        public event Action OnChangeGraphicClicked;
        public event Action OnSoundButtonClicked;

        private bool _soundState;
        
        private void OnEnable()
        {
            _hintButton.gameObject.SetActive(false);
            
            _settingsButton.onClick.AddListener(OnSettingsClickedHandler);
            _hintButton.onClick.AddListener(OnHintClickedHandler);
            _changeGraphicButton.onClick.AddListener(OnChangeGraphicClickedHandler);
            _soundButton.onClick.AddListener(OnSoundClickedHandler);
        }

        private void OnDisable()
        {
            _settingsButton.onClick.RemoveListener(OnSettingsClickedHandler);
            _hintButton.onClick.RemoveListener(OnHintClickedHandler);
            _changeGraphicButton.onClick.RemoveListener(OnChangeGraphicClickedHandler);
            _soundButton.onClick.RemoveListener(OnSoundClickedHandler);
        }
        
        public void EnableHint()
        {
            _hintButton.gameObject.SetActive(true);
        }

        public void UpdatePassedTime(float passedTime)
        {
            _passedTimeText.text = $"Time: {passedTime:F1}";
        }
        
        public void SetSoundState(bool isEnabled)
        {
            Sprite sprite = isEnabled ? _activeSoundSprite : _inactiveSoundSprite;
            _soundButton.image.sprite = sprite;
        }
    
        private void OnSettingsClickedHandler()
        {
            OnSettingsClicked?.Invoke();
        }
        
        private void OnHintClickedHandler()
        {
            OnHintClicked?.Invoke();
        }
        
        private void OnChangeGraphicClickedHandler()
        {
            OnChangeGraphicClicked?.Invoke();
        }
        
        private void OnSoundClickedHandler()
        {
            OnSoundButtonClicked?.Invoke();
        }
    }
}
