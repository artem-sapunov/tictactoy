using System.Collections.Generic;
using AssetManagement;
using Audio;
using Constants;
using Cysharp.Threading.Tasks;
using Gameplay.Systems.GameplayTime;
using Gameplay.Systems.Hint;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.AssetsResources;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Players;
using UI.Gameplay.Settings;
using UI.Gameplay.Undo;
using UnityEngine;
using Zenject;

namespace UI.Gameplay.Hud
{
    public class TicTacToeHudPresenter : MonoBehaviour, ITicTacToeHudPresenter
    {
        [SerializeField] private TicTacToeHudView _view;
        [SerializeField] private UndoPresenter _undoPresenter;
        
        [Inject] private IGameplayTime _gameplayTime;
        [Inject] private UIManager _uiManager;
        [Inject] private IHintSystem _hintSystem;
        [Inject] private ITicTacToeController _ticTacToeController;
        [Inject] private IAssetsLoader _assetsLoader;
        [Inject] private ITicTacToeResources _tacToeResources;
        [Inject] private AudioSystem _audioSystem;

        private SymbolType _currentTurn;
        private bool _graphicsLoading;
        private bool _graphicChanged;

        private void OnEnable()
        {
            _ticTacToeController.OnPlayerTurn += OnPlayerTurnHandler;
            
            _gameplayTime.OnPassedTimeChanged += OnPassedTimeChangedHandler;
            
            _view.OnSettingsClicked += OnSettingsClickedHandler;
            _view.OnHintClicked += OnHintClickedHandler;
            _view.OnChangeGraphicClicked += OnChangeGraphicClickedHandler;
            _view.OnSoundButtonClicked += OnSoundClickedHandler;
        }

        private void OnDisable()
        {
            _ticTacToeController.OnPlayerTurn -= OnPlayerTurnHandler;
            
            _gameplayTime.OnPassedTimeChanged -= OnPassedTimeChangedHandler;
            
            _view.OnSettingsClicked -= OnSettingsClickedHandler;
            _view.OnHintClicked -= OnHintClickedHandler;
            _view.OnChangeGraphicClicked -= OnChangeGraphicClickedHandler;
            _view.OnSoundButtonClicked -= OnSoundClickedHandler;
        }

        public void Init()
        {
            _view.SetSoundState(_audioSystem.IsEnabled);
        }

        public void EnableUndo(IReadOnlyCollection<SymbolType> allowedPlayers)
        {
            _undoPresenter.Initialize(allowedPlayers);
        }

        public void EnableHint(IReadOnlyCollection<SymbolType> allowedPlayers)
        {
            _hintSystem.Init(allowedPlayers);
            _view.EnableHint();
        }

        private void OnPassedTimeChangedHandler(float passedTime)
        {
            _view.UpdatePassedTime(passedTime);
        }
        
        private void OnSettingsClickedHandler()
        {
            _uiManager.Show<SettingsView>();
        }
        
        private void OnHintClickedHandler()
        {
            _hintSystem.ShowHint(_currentTurn);
        }

        private void OnSoundClickedHandler()
        {
            _audioSystem.Switch();
            _view.SetSoundState(_audioSystem.IsEnabled);
        }
        
        private void OnChangeGraphicClickedHandler()
        {
            if (_graphicsLoading)
            {
                return;
            }

            ChangeGraphic().Forget();
        }
        
        private void OnPlayerTurnHandler(IPlayer player)
        {
            _currentTurn = player.SymbolType;
        }

        private async UniTaskVoid ChangeGraphic()
        {
            _graphicsLoading = true;

            if (_graphicChanged)
            {
                await _tacToeResources.Init();
                _tacToeResources.SetAssetsProvider(_tacToeResources.AssetsProvider);
            }
            else
            {
                TicTacToeAssetsProvider assetsProvider = await _assetsLoader.LoadAssetFromBundle<TicTacToeAssetsProvider>(BundlesNames.Gameplay, AssetsPath.AssetsProviderBundlePath);
                _tacToeResources.SetAssetsProvider(assetsProvider);
            }
            
            _graphicsLoading = false;
            _graphicChanged = !_graphicChanged;
        }
    }
}