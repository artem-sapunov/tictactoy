using System.Collections.Generic;
using Gameplay.TicTacToe;

namespace UI.Gameplay.Hud
{
    public interface ITicTacToeHudPresenter
    {
        void Init();
        void EnableUndo(IReadOnlyCollection<SymbolType> allowedPlayers);
        void EnableHint(IReadOnlyCollection<SymbolType> allowedPlayers);
    }
}