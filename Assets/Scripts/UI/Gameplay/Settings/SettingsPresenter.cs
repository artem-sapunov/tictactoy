using States;
using UnityEngine;
using Zenject;

namespace UI.Gameplay.Settings
{
    public class SettingsPresenter : MonoBehaviour
    {
        [Inject] private IGameStateMachine _gameStateMachine;
        [Inject] private UIManager _uiManager;
        
        [SerializeField] private SettingsView _settingsView;

        private void OnEnable()
        {
            _settingsView.OnRestartClicked += OnRestartClickedHandler;
            _settingsView.OnBackToMainMenuClicked += OnBackToMainMenuClickedHandler;
            _settingsView.OnExitButtonClicked += OnExitClickedHandler;
        }

        private void OnDisable()
        {
            _settingsView.OnRestartClicked -= OnRestartClickedHandler;
            _settingsView.OnBackToMainMenuClicked -= OnBackToMainMenuClickedHandler;
            _settingsView.OnExitButtonClicked -= OnExitClickedHandler;
        }
        
        private void OnRestartClickedHandler()
        {
            _uiManager.Hide<SettingsView>();
            _gameStateMachine.Enter<RestartGameState>();
        }
        
        private void OnBackToMainMenuClickedHandler()
        {
            _uiManager.Hide<SettingsView>();
            _gameStateMachine.Enter<ExitGameplayState>();
        }

        private void OnExitClickedHandler()
        {
            _uiManager.Hide<SettingsView>();
        }
    }
}