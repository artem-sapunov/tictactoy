using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Gameplay.Settings
{
    public class SettingsView : UIWindow
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _backToMainMenuButton;
        [SerializeField] private Button _exitButton;

        public event Action OnRestartClicked;
        public event Action OnBackToMainMenuClicked;
        public event Action OnExitButtonClicked;
        
        private void OnEnable()
        {
            _restartButton.onClick.AddListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.AddListener(BackToMainMenuClickedHandler);
            _exitButton.onClick.AddListener(ExitClickedHandler);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.RemoveListener(BackToMainMenuClickedHandler);
            _exitButton.onClick.RemoveListener(ExitClickedHandler);
        }

        private void RestartClickedHandler()
        {
            OnRestartClicked?.Invoke();
        }

        private void BackToMainMenuClickedHandler()
        {
            OnBackToMainMenuClicked?.Invoke();
        }
        
        private void ExitClickedHandler()
        {
            OnExitButtonClicked?.Invoke();
        }
    }
}