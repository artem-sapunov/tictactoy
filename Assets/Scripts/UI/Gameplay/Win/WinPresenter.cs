using States;
using UnityEngine;
using Zenject;

namespace UI.Gameplay.Win
{
    public class WinPresenter : MonoBehaviour
    {
        [Inject] private IGameStateMachine _gameStateMachine;
        [Inject] private UIManager _uiManager;
        
        [SerializeField] private WinView _view;

        private void OnEnable()
        {
            _view.OnRestartClicked += OnRestartClickedHandler;
            _view.OnBackToMainMenuClicked += OnBackToMainMenuClickedHandler;
        }

        private void OnDisable()
        {
            _view.OnRestartClicked -= OnRestartClickedHandler;
            _view.OnBackToMainMenuClicked -= OnBackToMainMenuClickedHandler;
        }
        
        private void OnRestartClickedHandler()
        {
            _uiManager.Hide<WinView>();
            _gameStateMachine.Enter<RestartGameState>();
        }
        
        private void OnBackToMainMenuClickedHandler()
        {
            _uiManager.Hide<WinView>();
            _gameStateMachine.Enter<ExitGameplayState>();
        }
    }
}