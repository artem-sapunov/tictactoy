using System;
using System.Threading.Tasks;
using Audio;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Gameplay.Win
{
    public class WinView : UIWindow
    {
        [SerializeField] private TMP_Text _winPlayerText;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _backToMainMenuButton;
        [SerializeField] private GameObject _winLinePrefab;
        [SerializeField] private Animation _animation;
        [SerializeField] private AudioClip _winLineSound;
        [SerializeField] private string _appearAnimationName;
        [SerializeField] private float _appearDelay = 2f;

        [Inject] private IGrid _grid;
        [Inject] private AudioSystem _audioSystem;

        private GameObject _winLine;

        public event Action OnRestartClicked;
        public event Action OnBackToMainMenuClicked;
        
        private void OnEnable()
        {
            _restartButton.onClick.AddListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.AddListener(BackToMainMenuClickedHandler);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(RestartClickedHandler);
            _backToMainMenuButton.onClick.RemoveListener(BackToMainMenuClickedHandler);
        }
        
        public void SetWinPlayerName(string playerName)
        {
            _winPlayerText.text = $"{playerName} wins";
        }

        public void SetMatch(IMatch match)
        {
            Animate(match).Forget();
        }

        private void RestartClickedHandler()
        {
            Destroy(_winLine);
            OnRestartClicked?.Invoke();
        }

        private void BackToMainMenuClickedHandler()
        {
            Destroy(_winLine);
            OnBackToMainMenuClicked?.Invoke();
        }

        private async UniTaskVoid Animate(IMatch match)
        {
            Vector3 startPosition = _grid.GetWorldPosition(match.MatchPositions[0]);
            Vector3 centerPosition = _grid.GetWorldPosition(match.MatchPositions[1]);
            Vector3 endPosition = _grid.GetWorldPosition(match.MatchPositions[2]);
            Vector3 direction = endPosition - startPosition;

            _winLine = Instantiate(_winLinePrefab, centerPosition, Quaternion.identity);
            float angle = Vector3.SignedAngle(direction, Vector3.right, Vector3.up);
            _winLine.transform.rotation = Quaternion.Euler(0, 0, angle);
            _audioSystem.Play(_winLineSound);

            await Task.Delay((int)(_appearDelay * 1000));

            _animation.Play(_appearAnimationName);
        }
    }
}