using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [Inject] private IInjector _injector;
        
        [SerializeField] private RectTransform _windowsRoot;
        [SerializeField] private List<UIWindow> _windows;

        private Dictionary<Type, UIWindow> _activeWindows = new Dictionary<Type, UIWindow>();
        
        public T Show<T>() where T : UIWindow
        {
            Type windowType = typeof(T);
            T window = (T) _windows.Find(x => x.GetType() == windowType);
            T spawnedWindow = Instantiate(window, _windowsRoot);
            _injector.Inject(spawnedWindow.gameObject);
            _activeWindows.Add(windowType, spawnedWindow);
            return spawnedWindow;
        }

        public void Hide<T>() where T : UIWindow
        {
            Type windowType = typeof(T);
            if (_activeWindows.TryGetValue(windowType, out UIWindow window))
            {
                _activeWindows.Remove(windowType);
                Destroy(window.gameObject);
            }
        }
    }
}