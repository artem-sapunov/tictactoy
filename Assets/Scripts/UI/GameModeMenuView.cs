using System;
using Gameplay.TicTacToe.GameInitializer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameModeMenuView : UIWindow
    {
        [SerializeField] private Button _startButton;
        [SerializeField] private TMP_Dropdown _modesDropdown;
        [SerializeField] private GameModeType _defaultMode;

        public event Action<GameModeType> OnSelectGameMode;

        private GameModeType _selectedModeType;
        
        private void OnEnable()
        {
            _startButton.onClick.AddListener(OnStartClickedHandler);
            
            _modesDropdown.options.Clear();
            string[] gameModes = Enum.GetNames(typeof(GameModeType));
            foreach (string gameMode in gameModes)
            {
                _modesDropdown.options.Add(new TMP_Dropdown.OptionData(gameMode));
            }
            _modesDropdown.value = (int)_defaultMode;
            _modesDropdown.onValueChanged.AddListener(OnModeChangedHandler);
        }

        private void OnDisable()
        {
            _startButton.onClick.RemoveListener(OnStartClickedHandler);
            _modesDropdown.onValueChanged.RemoveListener(OnModeChangedHandler);
        }
        
        private void OnModeChangedHandler(int mode)
        {
            _selectedModeType = (GameModeType)mode;
        }

        private void OnStartClickedHandler()
        {
            OnSelectGameMode?.Invoke(_selectedModeType);
        }
    }
}