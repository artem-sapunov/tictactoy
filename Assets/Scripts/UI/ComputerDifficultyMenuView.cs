using System;
using Gameplay.TicTacToe.Players.AI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ComputerDifficultyMenuView : UIWindow
    {
        [SerializeField] private ComputerDifficulty _defaultDifficulty;
        [SerializeField] private TMP_Dropdown _dropdown;
        [SerializeField] private Button _startgameButton;

        private int _selectedDifficulty;

        public event Action<ComputerDifficulty> OnStartGame;
        
        private void Awake()
        {
            _dropdown.options.Clear();
            string[] difficulties = Enum.GetNames(typeof(ComputerDifficulty));
            foreach (string difficulty in difficulties)
            {
                _dropdown.options.Add(new TMP_Dropdown.OptionData(difficulty));
            }
            _dropdown.value = (int)_defaultDifficulty;
            _dropdown.onValueChanged.AddListener(OnDifficultyChanged);
            
            _startgameButton.onClick.AddListener(OnStartGameClicked);
        }

        private void OnDifficultyChanged(int difficulty)
        {
            _selectedDifficulty = difficulty;
        }

        private void OnStartGameClicked()
        {
            OnStartGame?.Invoke((ComputerDifficulty)_selectedDifficulty);
        }
    }
}