using UnityEngine;

namespace Audio
{
    public class AudioSystem : MonoBehaviour
    {
        private const string IsEnabledKey = "IsEnabled";
        
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioSource _backgroundAudioSource;

        public bool IsEnabled { get; private set; }
        
        private void OnEnable()
        {
            IsEnabled = GetBool(PlayerPrefs.GetInt(IsEnabledKey, 1));
            UpdateState();
        }

        public void Play(AudioClip audioClip)
        {
            _audioSource.PlayOneShot(audioClip);
        }

        public void Switch()
        {
            IsEnabled = !IsEnabled;
            PlayerPrefs.SetInt(IsEnabledKey, ToInt(IsEnabled));
            PlayerPrefs.Save();
            UpdateState();
        }

        private void UpdateState()
        {
            _audioSource.mute = !IsEnabled;
            _backgroundAudioSource.mute = !IsEnabled;
        }

        private bool GetBool(int value)
        {
            return value == 1;
        }

        private int ToInt(bool value)
        {
            return value ? 1 : 0;
        }
    }
}