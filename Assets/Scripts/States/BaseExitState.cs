using Zenject;

namespace States
{
    public abstract class BaseExitState : IExitState, IInitializable
    {
        [Inject] private IGameStateMachine _gameStateMachine;

        protected IGameStateMachine GameStateMachine => _gameStateMachine;
        
        public void Initialize()
        {
            _gameStateMachine.Add(this);
        }
        
        public abstract void Exit();
    }
}