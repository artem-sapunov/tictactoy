using Gameplay.TicTacToe.Players.AI;
using UI;
using Zenject;

namespace States
{
    public class SelectDifficultyState : BaseState
    {
        [Inject] private UIManager _uiManager;
        
        private ComputerDifficultyMenuView _computerDifficultyMenu;
        
        public override void Enter()
        {
            _computerDifficultyMenu = _uiManager.Show<ComputerDifficultyMenuView>();
            _computerDifficultyMenu.OnStartGame += OnStartGameHandler;
        }
        
        public override void Exit()
        {
            _computerDifficultyMenu.OnStartGame -= OnStartGameHandler;
            _uiManager.Hide<ComputerDifficultyMenuView>();
        }
        
        private void OnStartGameHandler(ComputerDifficulty difficulty)
        {
            GameStateMachine.Enter<InitializePlayerVsComputerState, ComputerDifficulty>(difficulty);
        }
    }
}