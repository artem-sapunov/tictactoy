using Gameplay.TicTacToe.GameInitializer;
using Gameplay.TicTacToe.Players.AI;


namespace States
{
    public class InitializePlayerVsComputerState : BasePayloadState<ComputerDifficulty>
    {
        public override void Enter(ComputerDifficulty difficulty)
        {
            TicTacToeInitializationData initializationData =
                new TicTacToeInitializationData(GameModeType.PlayerVsComputer, difficulty);
            
            GameStateMachine.Enter<InitializeGameplayState, TicTacToeInitializationData>(initializationData);
        }

        public override void Exit()
        {
        }
    }
}