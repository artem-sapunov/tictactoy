namespace States
{
    public abstract class BaseState : BaseExitState, IState
    {
        public abstract void Enter();
    }
}