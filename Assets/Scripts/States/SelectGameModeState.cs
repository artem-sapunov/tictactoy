using Constants;
using Gameplay.TicTacToe.GameInitializer;
using UI;
using Zenject;

namespace States
{
    public class SelectGameModeState : BaseState
    {
        [Inject] private SceneLoader _sceneLoader;
        [Inject] private UIManager _uiManager;
        [Inject] private Curtains _curtains;

        private GameModeMenuView _gameModeMenuView;
        
        public override async void Enter()
        {
            await _sceneLoader.LoadScene(SceneNames.MenuSceneName);
            _gameModeMenuView = _uiManager.Show<GameModeMenuView>();
            _gameModeMenuView.OnSelectGameMode += OnSelectGameModeHandler;
            _curtains.Hide();
        }
        
        public override void Exit()
        {
            _gameModeMenuView.OnSelectGameMode -= OnSelectGameModeHandler;
            _uiManager.Hide<GameModeMenuView>();
        }
        
        private void OnSelectGameModeHandler(GameModeType gameModeType)
        {
            if (gameModeType == GameModeType.PlayerVsPlayer)
            {
                GameStateMachine.Enter<InitializePlayerVsPlayerState>();
            }
            else if (gameModeType == GameModeType.ComputerVsComputer)
            {
                GameStateMachine.Enter<InitializeComputerVsComputerState>();
            }
            else
            {
                GameStateMachine.Enter<SelectDifficultyState>();
            }
        }
    }
}