using Gameplay.TicTacToe.GameInitializer;

namespace States
{
    public class InitializePlayerVsPlayerState : BaseState
    {
        public override void Enter()
        {
            TicTacToeInitializationData initializationData =
                new TicTacToeInitializationData(GameModeType.PlayerVsPlayer);
            
            GameStateMachine.Enter<InitializeGameplayState, TicTacToeInitializationData>(initializationData);
        }
        
        public override void Exit()
        {
        }
    }
}