using Constants;
using Factories;
using Gameplay.TicTacToe.AssetsResources;
using Gameplay.TicTacToe.GameInitializer;
using UI;
using Zenject;

namespace States
{
    public class InitializeGameplayState : BasePayloadState<TicTacToeInitializationData>
    {
        [Inject] private ITicTacToeInitializerFactory _factory;
        [Inject] private ITicTacToeResources _ticTacToeResources;
        [Inject] private SceneLoader _sceneLoader;
        [Inject] private Curtains _curtains;
        
        public override async void Enter(TicTacToeInitializationData initializationData)
        {
            _curtains.Show();
            
            await _ticTacToeResources.Init();
            await _sceneLoader.LoadScene(SceneNames.GameplayScene);
            
            ITicTacToeInitializer initializer = _factory.Create(initializationData);
            await initializer.Init();
            
            GameStateMachine.Enter<GameLoopState>();
        }

        public override void Exit()
        {
            
        }
    }
}