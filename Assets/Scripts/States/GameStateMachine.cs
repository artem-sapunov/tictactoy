﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace States
{
    public class GameStateMachine : IGameStateMachine
    {
        private IExitState _activeState;
        private Dictionary<Type, IExitState> _states = new Dictionary<Type, IExitState>();

        public void Add(IExitState state)
        {
            _states.Add(state.GetType(), state);
        }

        public void Enter<TState>() where TState : class, IState
        {
            var state = ChangeState<TState>();
            state.Enter();
        }

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadState<TPayload>
        {
            var state = ChangeState<TState>();
            state.Enter(payload);
        }

        private TState ChangeState<TState>() where TState : class, IExitState
        {
            if (_activeState != null)
            {
                Debug.Log($"Exit state: {_activeState}");
                _activeState?.Exit();
            }

            var newState = _states[typeof(TState)] as TState;
            _activeState = newState;

            Debug.Log($"Enter state: {_activeState}");

            return newState;
        }
    }
}