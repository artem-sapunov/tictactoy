﻿namespace States
{
    public interface IState : IExitState
    {
        void Enter();
    }
}