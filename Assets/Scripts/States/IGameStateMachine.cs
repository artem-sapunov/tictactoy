﻿namespace States
{
    public interface IGameStateMachine
    {
        void Add(IExitState state);
        void Enter<TState>() where TState : class, IState;
        void Enter<TState, TPayLoad>(TPayLoad payLoad) where TState : class, IPayloadState<TPayLoad>;
    }
}