using Gameplay.TicTacToe.GameInitializer;
using UI;
using Zenject;

namespace States
{
    public class InitializeComputerVsComputerState : BaseState
    {
        [Inject] private Curtains _curtains;
        
        public override void Enter()
        {
            _curtains.Show();

            TicTacToeInitializationData initializationData =
                new TicTacToeInitializationData(GameModeType.ComputerVsComputer);
            
            GameStateMachine.Enter<InitializeGameplayState, TicTacToeInitializationData>(initializationData);
        }
        
        public override void Exit()
        {
        }
    }
}