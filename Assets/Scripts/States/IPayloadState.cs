﻿namespace States
{
    public interface IPayloadState<in T> : IExitState
    {
        void Enter(T payload);
    }
}