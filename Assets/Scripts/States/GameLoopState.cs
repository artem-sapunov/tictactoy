﻿using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.MatchLogic;
using Gameplay.TicTacToe.Players;
using UI;
using UI.Gameplay.Draw;
using UI.Gameplay.Win;
using Zenject;

namespace States
{
    public class GameLoopState : BaseState
    {
        [Inject] private Curtains _curtains;
        [Inject] private UIManager _uiManager;
        [Inject] private IInjector _injector;
        
        private ITicTacToeController _ticTacToeController;
        
        public override void Enter()
        {
            _ticTacToeController = _injector.CurrentContainer.Resolve<ITicTacToeController>();
            
            _curtains.Hide();
            
            _ticTacToeController.OnWinPlayer += ControllerOnOnWinPlayer;
            _ticTacToeController.OnDraw += ControllerOnOnDraw;
            
            _ticTacToeController.StartGame();
        }
        
        public override void Exit()
        {
            _ticTacToeController.OnWinPlayer -= ControllerOnOnWinPlayer;
            _ticTacToeController.OnDraw -= ControllerOnOnDraw;
        }

        private void ControllerOnOnDraw()
        {
            _uiManager.Show<DrawView>();
        }

        private void ControllerOnOnWinPlayer(IPlayer player, IMatch match)
        {
            WinView winView = _uiManager.Show<WinView>();
            winView.SetWinPlayerName(player.Name);
            winView.SetMatch(match);
        }
    }
}