using Gameplay.TicTacToe.Controller;
using Zenject;

namespace States
{
    public class RestartGameState : BaseState
    {
        [Inject] private IInjector _injector;
        
        public override void Enter()
        {
            ITicTacToeController ticTacToeController = _injector.CurrentContainer.Resolve<ITicTacToeController>();
            ticTacToeController.Reset();
            GameStateMachine.Enter<GameLoopState>();
        }

        public override void Exit()
        {
        }
    }
}