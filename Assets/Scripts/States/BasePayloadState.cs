namespace States
{
    public abstract class BasePayloadState<T> : BaseExitState, IPayloadState<T>
    {
        public abstract void Enter(T payload);
    }
}