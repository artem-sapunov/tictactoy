﻿namespace States
{
    public interface IExitState
    {
        void Exit();
    }
}