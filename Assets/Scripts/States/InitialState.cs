﻿namespace States
{
    public class InitialState : BaseState
    {
        public override void Enter()
        {
            GameStateMachine.Enter<SelectGameModeState>();
        }

        public override void Exit()
        {
        }
    }
}