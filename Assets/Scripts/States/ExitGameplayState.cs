using UI;
using Zenject;

namespace States
{
    public class ExitGameplayState : BaseState
    {
        [Inject] private Curtains _curtains;
        
        public override void Enter()
        {
            _curtains.Show();
            
            GameStateMachine.Enter<SelectGameModeState>();
        }
        
        public override void Exit()
        {
        }
    }
}