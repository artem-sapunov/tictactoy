﻿using States;
using UnityEngine;
using Zenject;

public class EntryPoint : MonoBehaviour
{
    [Inject] private IGameStateMachine _gameStateMachine;

    void Start()
    {
        _gameStateMachine.Enter<InitialState>();
    }
}
