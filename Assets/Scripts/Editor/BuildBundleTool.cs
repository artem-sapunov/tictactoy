﻿using System.Collections.Generic;
using System.IO;
using Constants;
using Gameplay.TicTacToe.AssetsResources;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class BuildBundleTool : EditorWindow
    {
        private const string PathKey = "BuildBundleTool.Path";
        
        private string _path;
        private Vector2 _scrollPos;
        private string _bundleName;
        private Sprite _xSymbol;
        private Sprite _oSymbol;
        private Sprite _background;
        
        
        [MenuItem("Tools/Build Bundle Tool")]
        static void CreateWindow()
        {
            BuildBundleTool window = (BuildBundleTool)GetWindowWithRect(typeof(BuildBundleTool), new Rect(0, 0, 1000, 500));
            window.minSize = new Vector2(400, 120);
            window.maxSize = new Vector2(4000, 4000);
        }

        private void OnEnable()
        {
            _path = EditorPrefs.GetString(PathKey);

            if (string.IsNullOrEmpty(_path))
            {
                _path = Application.streamingAssetsPath;
            }
        }

        private void OnGUI()
        {
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
            {
                _bundleName = EditorGUILayout.TextField("Bundle name", _bundleName);
                _xSymbol = (Sprite) EditorGUILayout.ObjectField(new GUIContent("X symbol"), _xSymbol, typeof(Sprite), false);
                _oSymbol = (Sprite) EditorGUILayout.ObjectField(new GUIContent("O symbol"), _oSymbol, typeof(Sprite), false);
                _background = (Sprite) EditorGUILayout.ObjectField(new GUIContent("Background"), _background, typeof(Sprite), false);
            
                DrawPath();

                if (GUILayout.Button("Build"))
                {
                    if (!string.IsNullOrEmpty(_bundleName))
                    {
                        BuildAssetBundle(_path);
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }

        private void DrawPath()
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUI.BeginChangeCheck();
                {
                    _path = EditorGUILayout.TextField("Path", _path);
                }
                if (EditorGUI.EndChangeCheck())
                {
                    SavePath(_path);
                }
                
                if (GUILayout.Button("Browse..."))
                {
                    string selectedPath = EditorUtility.OpenFolderPanel("Select folder", _path, "");
                    if (!string.IsNullOrEmpty(selectedPath))
                    {
                        _path = selectedPath;
                        SavePath(_path);
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        private void BuildAssetBundle(string buildPath)
        {
            TicTacToeAssetsProvider assetsProvider = CreateInstance<TicTacToeAssetsProvider>();
            assetsProvider.SetAssets(_xSymbol, _oSymbol, _background);
            AssetDatabase.CreateAsset(assetsProvider, AssetsPath.AssetsProviderBundlePath);
                    
            AssetBundleBuild assetBundleBuild = new AssetBundleBuild()
            {
                assetBundleName = _bundleName,
                assetNames = new []{ AssetsPath.AssetsProviderBundlePath }
            };
            
            if (!Directory.Exists(buildPath))
            {
                Directory.CreateDirectory(buildPath);
            }

            BuildPipeline.BuildAssetBundles(buildPath, new[] { assetBundleBuild },
                BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);

            AssetDatabase.DeleteAsset(AssetsPath.AssetsProviderBundlePath);
            AssetDatabase.Refresh();
        }

        private void SavePath(string path)
        {
            EditorPrefs.SetString(PathKey, path);
        }
    }
}
