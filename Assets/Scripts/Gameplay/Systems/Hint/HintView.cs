using System.Linq;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.Grid.Cells;
using Gameplay.TicTacToe.Players;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gameplay.Systems.Hint
{
    public class HintView : MonoBehaviour
    {
        [Inject] private IHintSystem _hintSystem;
        [Inject] private ITicTacToeController _ticTacToeController;
        [Inject] private IGrid _grid;

        [SerializeField] private Button _hintButton;

        private ICell _hintCell;

        private void OnEnable()
        {
            _hintSystem.HintChanged += OnHintChanged;
            
            _ticTacToeController.OnPlayerTurn += OnPlayerTurnHandler;
            _ticTacToeController.OnPlayerMoved += OnPlayerMovedHandler;
            _ticTacToeController.OnUndo += OnUndoHandler;
        }

        private void OnDisable()
        {
            _hintSystem.HintChanged -= OnHintChanged;
            
            _ticTacToeController.OnPlayerTurn -= OnPlayerTurnHandler;
            _ticTacToeController.OnPlayerMoved -= OnPlayerMovedHandler;
            _ticTacToeController.OnUndo -= OnUndoHandler;
        }
        
        private void OnUndoHandler()
        {
            DisableHint();
        }

        private void OnPlayerMovedHandler(Vector2Int position, IPlayer player)
        {
            DisableHint();
        }

        private void DisableHint()
        {
            if (_hintCell != null)
            {
                _hintCell.SetHint(false);
                _hintCell = null;
            }
        }

        private void OnPlayerTurnHandler(IPlayer player)
        {
            bool isHintAllowed = _hintSystem.AllowedPlayers.Contains(player.SymbolType);
            _hintButton.interactable = isHintAllowed;
        }

        private void OnHintChanged(HintData hintData)
        {
            if (hintData != null)
            {
                _hintCell = _grid.GetCell(hintData.TargetPosition);
                _hintCell.SetHint(true);
            }
        }
    }
}