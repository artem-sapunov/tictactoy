using System;
using System.Collections.Generic;
using Gameplay.TicTacToe;

namespace Gameplay.Systems.Hint
{
    public interface IHintSystem
    {
        event Action<HintData> HintChanged;
        IReadOnlyCollection<SymbolType> AllowedPlayers { get; }
        void Init(IReadOnlyCollection<SymbolType> allowedPlayers);
        void ShowHint(SymbolType symbolType);
    }
}