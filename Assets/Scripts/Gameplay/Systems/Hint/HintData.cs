using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Systems.Hint
{
    public class HintData
    {
        public IReadOnlyCollection<Vector2Int> Sequence { get; }
        public Vector2Int TargetPosition { get; }

        public HintData(IReadOnlyCollection<Vector2Int> sequence, Vector2Int targetPosition)
        {
            Sequence = sequence;
            TargetPosition = targetPosition;
        }
    }
}