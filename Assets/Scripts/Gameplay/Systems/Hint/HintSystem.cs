using System;
using System.Collections.Generic;
using System.Linq;
using Gameplay.TicTacToe;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;
using Zenject;

namespace Gameplay.Systems.Hint
{
    public class HintSystem : IHintSystem
    {
        [Inject] private IGrid _grid;
        
        private ICell _hintCell;
        private List<HintData> _hintDatas;
        private int _maxCount;

        public event Action<HintData> HintChanged;

        public IReadOnlyCollection<SymbolType> AllowedPlayers { get; private set; }

        public void Init(IReadOnlyCollection<SymbolType> allowedPlayers)
        {
            _hintDatas = new List<HintData>(_grid.Height * _grid.Width);
            AllowedPlayers = allowedPlayers;
        }

        public void ShowHint(SymbolType symbolType)
        {
            if (!AllowedPlayers.Contains(symbolType))
            {
                return;
            }
            
            HintData hintData = GetHintData(symbolType);
            
            if (hintData != null)
            {
                HintChanged?.Invoke(hintData);
            }
        }

        private HintData GetHintData(SymbolType symbolType)
        {
            CollectHintDatas(symbolType);

            HintData hintData = SelectBestHintData();
            
            return hintData;
        }

        private void CollectHintDatas(SymbolType symbolType)
        {
            _hintDatas.Clear();
            _maxCount = 0;
            
            List<Vector2Int> sequence = new List<Vector2Int>(2);
            
            for (int x = 0; x < _grid.Width; x++)
            {
                for (int y = 0; y < _grid.Height; y++)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    ICell cell = _grid.GetCell(new Vector2Int(x, y));

                    if (cell.SymbolType != symbolType)
                    {
                        continue;
                    }

                    CollctAvailablePlace(position, symbolType, new Vector2Int(0, 1), sequence);
                    CollctAvailablePlace(position, symbolType, new Vector2Int(0, -1), sequence);
                    CollctAvailablePlace(position, symbolType, new Vector2Int(1, 0), sequence);
                    CollctAvailablePlace(position, symbolType, new Vector2Int(-1, 0), sequence);
                    CollctAvailablePlace(position, symbolType, new Vector2Int(1, 1), sequence);
                    CollctAvailablePlace(position, symbolType, new Vector2Int(-1, -1), sequence);
                }
            }
        }

        private HintData SelectBestHintData()
        {
            HintData hintData = _hintDatas.Find(x => x.Sequence.Count == _maxCount);
            return hintData;
        }
        
        private void CollctAvailablePlace(Vector2Int position, SymbolType symbolType, Vector2Int direction, List<Vector2Int> path)
        {
            path.Clear();
            path.Add(position);
            
            while (true)
            {
                position += direction;
                
                //Mismatch with type
                if (!IsMatch(position, symbolType))
                {
                    //Is cell empty
                    if (IsMatch(position, SymbolType.None))
                    {
                        if (path.Count > _maxCount)
                        {
                            _maxCount = path.Count;
                        }
                        
                        _hintDatas.Add(new HintData(new List<Vector2Int>(path), position));
                    }
                    
                    break;
                }
                
                path.Add(position);
            }
        }
        
        private bool IsMatch(Vector2Int position, SymbolType symbolType)
        {
            ICell cell = _grid.GetCell(position);

            if (cell != null)
            {
                return cell.SymbolType == symbolType;
            }

            return false;
        }
    }
}