using System;

namespace Gameplay.Systems.GameplayTime
{
    public interface IGameplayTime
    {
        event Action<float> OnPassedTimeChanged; 
        float PassedTime { get; }
    }
}