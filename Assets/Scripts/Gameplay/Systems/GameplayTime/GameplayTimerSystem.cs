using System;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.MatchLogic;
using Gameplay.TicTacToe.Players;
using UnityEngine;
using Zenject;

namespace Gameplay.Systems.GameplayTime
{
    public class GameplayTimerSystem : MonoBehaviour, IGameplayTime
    {
        [Inject] private ITicTacToeController _ticTacToeController;
        
        private CancellationTokenSource _cancellationTokenSource;

        public event Action<float> OnPassedTimeChanged;
        public float PassedTime { get; private set; }

        public void OnEnable()
        {
            _ticTacToeController.OnStart += OnStartGameHandler;
            _ticTacToeController.OnReset += OnResetGameHandler;
            _ticTacToeController.OnDraw += OnDrawGameHandler;
            _ticTacToeController.OnWinPlayer += OnWinPlayerGameHandler;
        }
        
        private void OnDisable()
        {
            _ticTacToeController.OnStart -= OnStartGameHandler;
            _ticTacToeController.OnReset -= OnResetGameHandler;
            _ticTacToeController.OnDraw -= OnDrawGameHandler;
            _ticTacToeController.OnWinPlayer -= OnWinPlayerGameHandler;
            
            if (_cancellationTokenSource != null && !_cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
        }

        private async UniTaskVoid StartTimeCounter(CancellationToken cancellationToken)
        {
            while (true)
            {
                await Task.Yield();

                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }
                
                PassedTime += Time.deltaTime;
                OnPassedTimeChanged?.Invoke(PassedTime);
            }
        }

        private void OnStartGameHandler()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            PassedTime = 0;
            StartTimeCounter(_cancellationTokenSource.Token).Forget();
        }

        private void OnResetGameHandler()
        {
            _cancellationTokenSource.Cancel();
        }

        private void OnDrawGameHandler()
        {
            _cancellationTokenSource.Cancel();
        }

        private void OnWinPlayerGameHandler(IPlayer player, IMatch match)
        {
            _cancellationTokenSource.Cancel();
        }
    }
}