using Gameplay.TicTacToe.AssetsResources;
using Gameplay.TicTacToe.Camera;
using Gameplay.TicTacToe.Grid;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Background
{
    public class BackgroundController : MonoBehaviour, IBackgroundController
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        
        [Inject] private IGrid _grid;
        [Inject] private ICameraController _cameraController;
        [Inject] private ITicTacToeResources _ticTacToeResources;

        private void OnEnable()
        {
            _ticTacToeResources.OnAssetProviderChanged += UpdateGraphic;
        }

        private void OnDisable()
        {
            _ticTacToeResources.OnAssetProviderChanged -= UpdateGraphic;
        }

        public void Init()
        {
            UpdateGraphic();
        }

        private void UpdatePosition()
        {
            Vector3 centerPosition = _grid.GetCenterPosition();
            centerPosition.z = transform.position.z;
            transform.position = centerPosition;
        }
        
        private void ResizeSpriteToScreen()
        {
            transform.localScale = new  Vector3(1,1,1);
    
            float width = _spriteRenderer.sprite.bounds.size.x;
            float height = _spriteRenderer.sprite.bounds.size.y;
    
            float worldScreenHeight = _cameraController.Camera.orthographicSize * 2.0f;
            float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            float max = Mathf.Max(worldScreenHeight, worldScreenWidth);
            Vector3 scale = new Vector3(max / width, max / height);

            transform.localScale = scale;
        }

        private void UpdateGraphic()
        {
            _spriteRenderer.sprite = _ticTacToeResources.AssetsProvider.Background;
            
            UpdatePosition();
            ResizeSpriteToScreen();
        }
    }
}