using System;
using UnityEngine;

namespace Gameplay.TicTacToe.Grid.Cells
{
    public interface ICell
    {
        event Action<ICell> OnClick;
        event Action<ICell, SymbolType, SymbolType> OnMarkChanged;
        
        Vector2Int Position { get; }
        SymbolType SymbolType { get; }
        void SetMark(SymbolType symbolType);
        void SetPosition(Vector2Int position);
        void SetHint(bool isActive);
    }
}