using System;
using Audio;
using Gameplay.TicTacToe.AssetsResources;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Gameplay.TicTacToe.Grid.Cells
{
    public class Cell : MonoBehaviour, ICell, IPointerClickHandler
    {
        [Inject] private ITicTacToeResources _ticTacToeResources;
        [Inject] private AudioSystem _audioSystem;
        
        [SerializeField] private SpriteRenderer _symbolSpriteRenderer;
        [SerializeField] private GameObject _hint;
        [SerializeField] private Animation _animation;
        [SerializeField] private string _appearAnimation;
        [SerializeField] private AudioClip _setMarkSound;
        
        public Vector2Int Position { get; private set; }
        public SymbolType SymbolType { get; private set; }
        
        public event Action<ICell> OnClick;
        public event Action<ICell, SymbolType, SymbolType> OnMarkChanged;

        private void OnEnable()
        {
            SetHint(false);
            _ticTacToeResources.OnAssetProviderChanged += UpdateSymbol;
        }

        private void OnDisable()
        {
            _ticTacToeResources.OnAssetProviderChanged -= UpdateSymbol;
        }

        public void SetMark(SymbolType symbolType)
        {
            if (SymbolType == symbolType)
            {
                return;
            }

            SymbolType oldType = SymbolType;
            SymbolType = symbolType;

            UpdateSymbol();

            if (symbolType != SymbolType.None)
            {
                _audioSystem.Play(_setMarkSound);
                _animation.Play(_appearAnimation);
            }
            
            OnMarkChanged?.Invoke(this, oldType, symbolType);
        }

        public void SetPosition(Vector2Int position)
        {
            Position = position;
        }

        public void SetHint(bool isActive)
        {
            _hint.SetActive(isActive);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(this);
        }

        private void UpdateSymbol()
        {
            Sprite symbol = _ticTacToeResources.AssetsProvider.GetSymbolSprite(SymbolType);
            _symbolSpriteRenderer.sprite = symbol;
        }
    }
}