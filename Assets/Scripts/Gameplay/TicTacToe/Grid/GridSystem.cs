using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssetManagement;
using Constants;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Grid
{
    public class GridSystem : MonoBehaviour, IGrid
    {
        [SerializeField] private UnityEngine.Grid _grid;
        [SerializeField] private Transform _cellsRoot;

        [Inject] private IAssetsLoader _assetsLoader;
        [Inject] private IInjector _injector;
        
        public event Action<Vector2Int> OnCellClicked;
        
        public int Height { get; private set; }
        public int Width { get; private set; }

        private ICell[,] _cells;
        private List<GameObject> _spawnedCells = new List<GameObject>();
        private int _filledCellsCounter;
        
        public async UniTask Init(int height, int width)
        {
            Height = height;
            Width = width;

            GameObject cellPrefab = await _assetsLoader.Load<GameObject>(AssetsPath.CellPath);
            cellPrefab.SetActive(false);

            _cells = new ICell[Height, Width];
            
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Vector3 position = _grid.GetCellCenterWorld(new Vector3Int(x, y, 0));
                    GameObject cellInstance = Instantiate(cellPrefab, position, Quaternion.identity, _cellsRoot);
                    _injector.Inject(cellInstance);
                    cellInstance.SetActive(true);
#if UNITY_EDITOR
                    cellInstance.name = $"Cell x:{x}, y:{y}";
#endif
                    ICell cell = cellInstance.GetComponent<ICell>();
                    cell.SetPosition(new Vector2Int(x, y));
                    
                    _cells[x, y] = cell;
                    cell.OnClick += OnCellClickedHandler;
                    cell.OnMarkChanged += OnMarkChangedHandler;
                }
            }
        }
        
        public void Dispose()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    ICell cell = _cells[x, y];
                    cell.OnClick -= OnCellClickedHandler;
                    cell.OnMarkChanged -= OnMarkChangedHandler;
                }
            }

            _cells = null;

            foreach (GameObject spawnedCell in _spawnedCells)
            {
                Destroy(spawnedCell);
            }
        }

        public void ClearField()
        {
            foreach (ICell cell in _cells)
            {
                cell.SetMark(SymbolType.None);
            }
        }

        public ICell GetCell(Vector2Int position)
        {
            if (position.x < 0 || position.x > Width - 1
                || position.y < 0 || position.y > Height - 1)
            {
                return null;
            }
            
            return _cells[position.x, position.y];
        }

        public bool HasNoEmptySpace()
        {
            return _filledCellsCounter == Width * Height;
        }

        public Vector2 CalculateGridSize()
        {
            float cellsSizeX = Width * _grid.cellSize.x;
            float gapSizeX = (Width - 1) * _grid.cellGap.x;
            
            float cellsSizeY = Height * _grid.cellSize.x;
            float gapSizeY = (Height - 1) * _grid.cellGap.x;
            
            return new Vector2(cellsSizeX + gapSizeX, cellsSizeY + gapSizeY);
        }

        public Vector3 GetWorldPosition(Vector2Int position)
        {
            return _grid.GetCellCenterWorld(new Vector3Int(position.x, position.y, 0));
        }

        public Vector3 GetCenterPosition()
        {
            Vector2Int leftBottomCorner = new Vector2Int(0, 0);
            Vector2Int rightUpCorner = new Vector2Int(Width - 1, Height - 1);
            Vector3 gridCenter = (GetWorldPosition(leftBottomCorner) + GetWorldPosition(rightUpCorner)) / 2;
            return gridCenter;
        }

        private void OnCellClickedHandler(ICell cell)
        {
            OnCellClicked?.Invoke(cell.Position);
        }

        private void OnMarkChangedHandler(ICell cell, SymbolType prevType, SymbolType newType)
        {
            if (newType == SymbolType.None)
            {
                _filledCellsCounter--;
            }
            else if(prevType == SymbolType.None)
            {
                _filledCellsCounter++;
            }
        }
    }
}