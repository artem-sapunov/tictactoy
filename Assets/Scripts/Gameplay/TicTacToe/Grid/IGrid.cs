using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;

namespace Gameplay.TicTacToe.Grid
{
    public interface IGrid : IDisposable
    {
        event Action<Vector2Int> OnCellClicked;
        
        int Height { get; }
        int Width { get; }
        
        UniTask Init(int height, int width);
        ICell GetCell(Vector2Int position);
        bool HasNoEmptySpace();
        void ClearField();
        Vector2 CalculateGridSize();
        Vector3 GetWorldPosition(Vector2Int position);
        Vector3 GetCenterPosition();
    }
}