using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Gameplay.TicTacToe.Players
{
    public interface IPlayer
    {
        string Name { get; }
        SymbolType SymbolType { get; }
        UniTask<Vector2Int> Move(CancellationToken cancellationToken);
    }
}