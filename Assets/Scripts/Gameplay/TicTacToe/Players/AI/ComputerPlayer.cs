using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Players.AI
{
    public class ComputerPlayer : IPlayer
    {
        [Inject] private IGrid _grid;

        public string Name { get; }
        public SymbolType SymbolType { get; }

        public ComputerPlayer(string name, SymbolType symbolType, ComputerDifficulty difficulty)
        {
            Name = name;
            SymbolType = symbolType;
        }
        
        public async UniTask<Vector2Int> Move(CancellationToken cancellationToken)
        {
            await UniTask.Delay(Random.Range(100, 1000), false, PlayerLoopTiming.Update, cancellationToken, true);

            List<Vector2Int> freeCells = new List<Vector2Int>();
            
            for (int x = 0; x < _grid.Width; x++)
            {
                for (int y = 0; y < _grid.Height; y++)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    ICell cell = _grid.GetCell(new Vector2Int(x, y));
                    
                    if (cell.SymbolType == SymbolType.None)
                    {
                        freeCells.Add(position);
                    }
                }
            }

            if (freeCells.Count > 0)
            {
                return freeCells[Random.Range(0, freeCells.Count)];
            }

            return Vector2Int.zero;
        }
    }
}