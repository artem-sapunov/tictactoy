namespace Gameplay.TicTacToe.Players.AI
{
    public enum ComputerDifficulty
    {
        Easy,
        Medium,
        Hard
    }
}