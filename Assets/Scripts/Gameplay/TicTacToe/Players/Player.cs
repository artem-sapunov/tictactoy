using System.Threading;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Players
{
    public class Player : IPlayer
    {
        [Inject] private IGrid _grid;

        public string Name { get; }
        public SymbolType SymbolType { get; }

        public Player(string name, SymbolType symbolType)
        {
            Name = name;
            SymbolType = symbolType;
        }
        
        public async UniTask<Vector2Int> Move(CancellationToken cancellationToken)
        {
            Vector2Int move = await WaitCellClick(cancellationToken);
            return move;
        }

        private async UniTask<Vector2Int> WaitCellClick(CancellationToken cancellationToken)
        { 
            UniTaskCompletionSource<Vector2Int> tcs = new UniTaskCompletionSource<Vector2Int>();
            
            _grid.OnCellClicked += OnClick;
            
            void OnClick(Vector2Int position)
            {
                if (_grid.GetCell(position).SymbolType == SymbolType.None)
                {
                    tcs.TrySetResult(position);
                }
            }

            (bool IsCanceled, Vector2Int Result) result = await tcs.Task.AttachExternalCancellation(cancellationToken).SuppressCancellationThrow();
            _grid.OnCellClicked -= OnClick;
            return result.Result;
        }
    }
}