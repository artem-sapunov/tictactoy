using System;
using Gameplay.TicTacToe.MatchLogic;
using Gameplay.TicTacToe.Players;
using UnityEngine;

namespace Gameplay.TicTacToe.Controller
{
    public interface ITicTacToeController : IDisposable
    {
        event Action<Vector2Int, IPlayer> OnPlayerMoved;
        event Action<IPlayer> OnPlayerTurn;
        event Action<IPlayer, IMatch> OnWinPlayer;
        event Action OnStart;
        event Action OnReset;
        event Action OnDraw;
        event Action OnUndo;

        void Init(IPlayer player1, IPlayer player2);
        void StartGame();
        void Reset();
        void Undo();
    }
}