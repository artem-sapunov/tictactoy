using System;
using System.Collections.Generic;
using System.Threading;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.MatchLogic;
using Gameplay.TicTacToe.Players;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Controller
{
    public class TicTacToeController : ITicTacToeController
    {
        [Inject] private IMatchChecker _matchChecker;
        [Inject] private IGrid _grid;
        
        private const int UndoMovesCount = 2;
        
        public event Action<Vector2Int, IPlayer> OnPlayerMoved;
        public event Action<IPlayer> OnPlayerTurn;
        public event Action<IPlayer, IMatch> OnWinPlayer;
        public event Action OnStart;
        public event Action OnReset;
        public event Action OnDraw;
        public event Action OnUndo;
        
        private CancellationTokenSource _cancellationTokenSource;
        private List<IPlayer> _players = new List<IPlayer>();
        private Stack<Vector2Int> _moves = new Stack<Vector2Int>();

        public void Init(IPlayer player1, IPlayer player2)
        {
            _players.Add(player1);
            _players.Add(player2);
        }
        
        public void StartGame()
        {
            RestartGameLoop();
            OnStart?.Invoke();
        }

        public void Reset()
        {
            _cancellationTokenSource.Cancel();
            _grid.ClearField();
            _moves.Clear();
            OnReset?.Invoke();
        }

        public void Undo()
        {
            if (_moves.Count > 1)
            {
                _cancellationTokenSource.Cancel();

                for (int i = 0; i < UndoMovesCount; i++)
                {
                    Vector2Int move = _moves.Pop();
                    _grid.GetCell(move).SetMark(SymbolType.None);
                }
                
                OnUndo?.Invoke();
                
                RestartGameLoop();
            }
        }
        
        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
        }

        private void RestartGameLoop()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            StartGameLoop(_cancellationTokenSource.Token);
        }

        private async void StartGameLoop(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                IPlayer player = _players[_moves.Count % _players.Count];
                
                OnPlayerTurn?.Invoke(player);
                
                Vector2Int move = await player.Move(cancellationToken);
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                
                _moves.Push(move);
                _grid.GetCell(move).SetMark(player.SymbolType);

                OnPlayerMoved?.Invoke(move, player);
                
                IMatch match = await _matchChecker.CheckMatch();

                if (match != null)
                {
                    OnWinPlayer?.Invoke(player, match);
                    
                    break;
                }

                if (_grid.HasNoEmptySpace())
                {
                    OnDraw?.Invoke();
                    break;
                }
            }
        }
    }
}