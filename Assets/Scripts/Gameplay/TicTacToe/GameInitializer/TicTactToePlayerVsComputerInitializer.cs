using Cysharp.Threading.Tasks;
using Gameplay.Systems.Hint;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Players;
using Gameplay.TicTacToe.Players.AI;
using UI.Gameplay.Hud;
using Zenject;

namespace Gameplay.TicTacToe.GameInitializer
{
    public class TicTactToePlayerVsComputerInitializer : BaseTicTacToeInitializer
    {
        [Inject] private IInjector _injector;
        [Inject] private ITicTacToeHudPresenter _hudPresenter;
        [Inject] private IHintSystem _hintSystem;
        
        private readonly ComputerDifficulty _difficulty;
        
        public TicTactToePlayerVsComputerInitializer(ComputerDifficulty difficulty)
        {
            _difficulty = difficulty;
        }

        protected override void InitController(ITicTacToeController controller)
        {
            IPlayer player1 = new Player("Player 1", SymbolType.X);
            _injector.Inject(player1);
            IPlayer player2 = new ComputerPlayer("Player 2", SymbolType.O, _difficulty);
            _injector.Inject(player2);
            controller.Init(player1, player2);
        }
        
        public override async UniTask Init()
        {
            await base.Init();
            
            SymbolType[] allowedPlayers = { SymbolType.X };
            _hudPresenter.EnableUndo(allowedPlayers);
            _hudPresenter.EnableHint(allowedPlayers);
            _hintSystem.Init(allowedPlayers);
        }
    }
}