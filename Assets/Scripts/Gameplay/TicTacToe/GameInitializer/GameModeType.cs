namespace Gameplay.TicTacToe.GameInitializer
{
    public enum GameModeType
    {
        PlayerVsPlayer,
        PlayerVsComputer,
        ComputerVsComputer
    }
}