using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Players;
using Gameplay.TicTacToe.Players.AI;
using Zenject;

namespace Gameplay.TicTacToe.GameInitializer
{
    public class TicTactToeComputerVsComputerInitializer : BaseTicTacToeInitializer
    {
        [Inject] private IInjector _injector;

        protected override void InitController(ITicTacToeController controller)
        {
            IPlayer player1 = new ComputerPlayer("Player 1", SymbolType.X, ComputerDifficulty.Easy);
            _injector.Inject(player1);
            IPlayer player2 = new ComputerPlayer("Player 2", SymbolType.O, ComputerDifficulty.Easy);
            _injector.Inject(player2);
            controller.Init(player1, player2);
        }
    }
}