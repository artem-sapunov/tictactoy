using Cysharp.Threading.Tasks;

namespace Gameplay.TicTacToe.GameInitializer
{
    public interface ITicTacToeInitializer
    {
        UniTask Init();
    }
}