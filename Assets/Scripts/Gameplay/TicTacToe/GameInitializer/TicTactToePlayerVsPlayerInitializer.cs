using Cysharp.Threading.Tasks;
using Gameplay.Systems.Hint;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Players;
using Zenject;

namespace Gameplay.TicTacToe.GameInitializer
{
    public class TicTactToePlayerVsPlayerInitializer : BaseTicTacToeInitializer
    {
        [Inject] private IHintSystem _hintSystem;
        [Inject] private IInjector _injector;
        
        protected override void InitController(ITicTacToeController controller)
        {
            IPlayer player1 = new Player("Player 1", SymbolType.X);
            _injector.Inject(player1);
            IPlayer player2 = new Player( "Player 2", SymbolType.O);
            _injector.Inject(player2);
            controller.Init(player1, player2);
        }

        public override async UniTask Init()
        {
            await base.Init();
            
            SymbolType[] allowedPlayers = { SymbolType.X };
            _hintSystem.Init(allowedPlayers);
        }
    }
}