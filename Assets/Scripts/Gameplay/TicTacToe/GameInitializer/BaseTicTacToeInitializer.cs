using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Background;
using Gameplay.TicTacToe.Camera;
using Gameplay.TicTacToe.Controller;
using Gameplay.TicTacToe.Grid;
using UI.Gameplay.Hud;
using Zenject;

namespace Gameplay.TicTacToe.GameInitializer
{
    public abstract class BaseTicTacToeInitializer : ITicTacToeInitializer
    {
        [Inject] private ITicTacToeController _ticTacToeController;
        [Inject] private IGrid _grid;
        [Inject] private ICameraController _cameraController;
        [Inject] private IBackgroundController _backgroundController;
        [Inject] private ITicTacToeHudPresenter _hudPresenter;
        
        public virtual async UniTask Init()
        {
            InitController(_ticTacToeController);
            
            await _grid.Init(3, 3);
            _cameraController.Init();
            _backgroundController.Init();
            _hudPresenter.Init();
        }

        protected abstract void InitController(ITicTacToeController controller);
    }
}