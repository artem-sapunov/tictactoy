using Gameplay.TicTacToe.Players.AI;

namespace Gameplay.TicTacToe.GameInitializer
{
    public class TicTacToeInitializationData
    {
        public GameModeType GameMode { get; private set; }
        public ComputerDifficulty ComputerDifficulty { get; private set; }

        public TicTacToeInitializationData(GameModeType gameMode)
        {
            GameMode = gameMode;
        }

        public TicTacToeInitializationData(GameModeType gameMode, ComputerDifficulty computerDifficulty)
        {
            GameMode = gameMode;
            ComputerDifficulty = computerDifficulty;
        }
    }
}