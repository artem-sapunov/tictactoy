using Gameplay.TicTacToe.Grid;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.Camera
{
    public class CameraController : MonoBehaviour, ICameraController
    {
        [SerializeField] private UnityEngine.Camera _camera;
        [SerializeField] private float _padding;
        
        [Inject] private IGrid _grid;

        public float OrthographicSize => _camera.orthographicSize;
        public UnityEngine.Camera Camera => _camera;

        public void Init()
        {
            UpdatePosition();
            UpdateOrthographicSize();
        }

        private void UpdatePosition()
        {
            Vector3 gridCenter = _grid.GetCenterPosition();
            gridCenter.z = -10;
            _camera.transform.position = gridCenter;
        }

        private void UpdateOrthographicSize()
        {
            Vector2 gridSize = _grid.CalculateGridSize();
            float maxSize = gridSize.x > gridSize.y ? gridSize.x : gridSize.y;
            _camera.orthographicSize = maxSize / 2 + _padding;
        }


    }
}