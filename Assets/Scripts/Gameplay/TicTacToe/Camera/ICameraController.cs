namespace Gameplay.TicTacToe.Camera
{
    public interface ICameraController
    {
        float OrthographicSize { get; }
        UnityEngine.Camera Camera { get; }
        void Init();
    }
}