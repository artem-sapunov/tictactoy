using System;
using AssetManagement;
using Constants;
using Cysharp.Threading.Tasks;
using Zenject;

namespace Gameplay.TicTacToe.AssetsResources
{
    public class TicTacToeResources : ITicTacToeResources, IDisposable
    {
        [Inject] private IAssetsLoader _assetsLoader;
        
        public event Action OnAssetProviderChanged;
        public ITicTacToeAssetsProvider AssetsProvider { get; private set; }
        
        public async UniTask Init()
        {
            AssetsProvider = await _assetsLoader.Load<TicTacToeAssetsProvider>(AssetsPath.AssetsProviderPath);
        }

        public void SetAssetsProvider(ITicTacToeAssetsProvider assetsProvider)
        {
            AssetsProvider = assetsProvider;
            OnAssetProviderChanged?.Invoke();
        }

        public void Dispose()
        {
            AssetsProvider = null;
        }
    }
}