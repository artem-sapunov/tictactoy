using UnityEngine;

namespace Gameplay.TicTacToe.AssetsResources
{
    [CreateAssetMenu(menuName = "TicTacToe/TicTacToeAssetsProvider")]
    public class TicTacToeAssetsProvider : ScriptableObject, ITicTacToeAssetsProvider
    {
        [SerializeField] private Sprite _xSymbol;
        [SerializeField] private Sprite _oSymbol;
        [SerializeField] private Sprite _background;

        
        public Sprite Background => _background;

        public Sprite GetSymbolSprite(SymbolType symbolType)
        {
            switch (symbolType)
            {
                case SymbolType.X:
                {
                    return _xSymbol;
                }
                case SymbolType.O:
                {
                    return _oSymbol;
                }
                default:
                {
                    return null;
                }
            }
        }

        public void SetAssets(Sprite xSymbol, Sprite oSymbol, Sprite background)
        {
            _xSymbol = xSymbol;
            _oSymbol = oSymbol;
            _background = background;
        }
    }
}