using UnityEngine;

namespace Gameplay.TicTacToe.AssetsResources
{
    public interface ITicTacToeAssetsProvider
    {
        Sprite Background { get; }

        Sprite GetSymbolSprite(SymbolType symbolType);
    }
}