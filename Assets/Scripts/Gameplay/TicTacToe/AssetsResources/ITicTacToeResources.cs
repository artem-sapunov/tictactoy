using System;
using Cysharp.Threading.Tasks;

namespace Gameplay.TicTacToe.AssetsResources
{
    public interface ITicTacToeResources
    {
        event Action OnAssetProviderChanged;
        ITicTacToeAssetsProvider AssetsProvider { get; }

        UniTask Init();
        void SetAssetsProvider(ITicTacToeAssetsProvider assetsProvider);
    }
}