namespace Gameplay.TicTacToe
{
    public enum SymbolType
    {
        None,
        X,
        O
    }
}