using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.TicTacToe.MatchLogic
{
    public class Match : IMatch
    {
        public SymbolType SymbolType { get; }
        public List<Vector2Int> MatchPositions { get; }

        public Match(SymbolType symbolType, List<Vector2Int> matchPositions)
        {
            SymbolType = symbolType;
            MatchPositions = matchPositions;
        }
    }
}