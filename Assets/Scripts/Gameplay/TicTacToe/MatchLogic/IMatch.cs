using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.TicTacToe.MatchLogic
{
    public interface IMatch
    {
        SymbolType SymbolType { get; }
        List<Vector2Int> MatchPositions { get; }
    }
}