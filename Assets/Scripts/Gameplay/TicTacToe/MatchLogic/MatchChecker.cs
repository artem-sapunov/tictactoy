using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gameplay.TicTacToe.Grid;
using Gameplay.TicTacToe.Grid.Cells;
using UnityEngine;
using Zenject;

namespace Gameplay.TicTacToe.MatchLogic
{
    public class MatchChecker : IMatchChecker
    {
        public const int WinCount = 3;
        
        [Inject] private IGrid _grid;
        
        public UniTask<IMatch> CheckMatch()
        {
            List<Vector2Int> sequence = new List<Vector2Int>(3);
            
            for (int x = 0; x < _grid.Width; x++)
            {
                for (int y = 0; y < _grid.Height; y++)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    ICell cell = _grid.GetCell(new Vector2Int(x, y));

                    if (cell.SymbolType == SymbolType.None)
                    {
                        continue;
                    }
                    
                    SymbolType symbolType = cell.SymbolType;

                    if (CalculateLinearMatch(position, cell.SymbolType, new Vector2Int(0, 1), sequence))
                    {
                        return UniTask.FromResult<IMatch>(new Match(symbolType, sequence));
                    }

                    if (CalculateLinearMatch(position, cell.SymbolType, new Vector2Int(1, 0), sequence))
                    {
                        return UniTask.FromResult<IMatch>(new Match(symbolType, sequence));
                    }
                    
                    if (CalculateLinearMatch(position, cell.SymbolType, new Vector2Int(1, 1), sequence))
                    {
                        return UniTask.FromResult<IMatch>(new Match(symbolType, sequence));
                    }
                    
                    if (CalculateLinearMatch(position, cell.SymbolType, new Vector2Int(-1, 1), sequence))
                    {
                        return UniTask.FromResult<IMatch>(new Match(symbolType, sequence));
                    }
                }
            }
            
            return UniTask.FromResult<IMatch>(null);
        }
        
        private bool CalculateLinearMatch(Vector2Int position, SymbolType symbolType, Vector2Int direction, List<Vector2Int> path)
        {
            path.Clear();
            path.Add(position);
            
            while (true)
            {
                position += direction;

                if (!IsMatch(position, symbolType))
                {
                    break;
                }
                
                path.Add(position);

                if (path.Count == WinCount)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsMatch(Vector2Int position, SymbolType symbolType)
        {
            ICell cell = _grid.GetCell(position);

            if (cell != null)
            {
                return cell.SymbolType == symbolType;
            }

            return false;
        }
    }
}