using Cysharp.Threading.Tasks;

namespace Gameplay.TicTacToe.MatchLogic
{
    public interface IMatchChecker
    {
        UniTask<IMatch> CheckMatch();
    }
}