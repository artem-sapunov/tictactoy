using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class Injector : IInjector
{
    [Inject] private SceneContextRegistry _sceneContextRegistry;

    public DiContainer CurrentContainer => GetContainer();
    
    public void Inject(GameObject gameObject)
    {
        DiContainer diContainer = GetContainer();
        diContainer.InjectGameObject(gameObject);
    }
    
    public void Inject(object obj)
    {
        DiContainer diContainer = GetContainer();
        diContainer.Inject(obj);
    }

    private DiContainer GetContainer()
    {
        Scene scene = SceneManager.GetActiveScene();
        DiContainer diContainer = _sceneContextRegistry.GetContainerForScene(scene);
        return diContainer;
    }
}