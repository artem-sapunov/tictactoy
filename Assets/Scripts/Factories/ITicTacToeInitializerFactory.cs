using Gameplay.TicTacToe.GameInitializer;

namespace Factories
{
    public interface ITicTacToeInitializerFactory
    {
        ITicTacToeInitializer Create(TicTacToeInitializationData initializationData);
    }
}