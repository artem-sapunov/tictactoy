using Gameplay.TicTacToe.GameInitializer;
using Gameplay.TicTacToe.Players.AI;
using Zenject;

namespace Factories
{
    public class TicTacToeInitializerFactory : ITicTacToeInitializerFactory
    {
        [Inject] private IInjector _injector;
        
        public ITicTacToeInitializer Create(TicTacToeInitializationData initializationData)
        {
            ITicTacToeInitializer initializer = null;
            
            switch (initializationData.GameMode)
            {
                case GameModeType.PlayerVsPlayer:
                {
                    initializer = new TicTactToePlayerVsPlayerInitializer();
                    break;
                }
                case GameModeType.PlayerVsComputer:
                {
                    initializer = new TicTactToePlayerVsComputerInitializer(initializationData.ComputerDifficulty);
                    break;
                }
                case GameModeType.ComputerVsComputer:
                {
                    initializer = new TicTactToeComputerVsComputerInitializer();
                    break;
                }
            }
            
            _injector.Inject(initializer);
            return initializer;
        }
    }
}