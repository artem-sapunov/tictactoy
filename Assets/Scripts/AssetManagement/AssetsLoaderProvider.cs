﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace AssetManagement
{
    public class AssetsLoaderProvider : IAssetsLoader
    {
        private Dictionary<string, AssetBundle> _loadedBundles = new Dictionary<string, AssetBundle>();
        
        public async UniTask<T> Load<T>(string path) where T : Object
        {
            Object asset = await Resources.LoadAsync<T>(path);
            return (T)asset;
        }

        public async UniTask<T> LoadAssetFromBundle<T>(string bundleName, string path) where T : Object
        {
            if (!_loadedBundles.TryGetValue(bundleName, out AssetBundle assetBundle))
            {
                string bundlePath = GetAssetBundlePath(bundleName);
                AssetBundleCreateRequest loadAssetBundleHandle = AssetBundle.LoadFromFileAsync(bundlePath);
                await loadAssetBundleHandle;
                assetBundle = loadAssetBundleHandle.assetBundle;
                _loadedBundles.Add(bundleName, assetBundle);
            }

            AssetBundleRequest loadHandle = assetBundle.LoadAssetAsync<T>(path);
            await loadHandle;

            return (T)loadHandle.asset;
        }
        
        private string GetAssetBundlePath(string bundleName)
        {
            return Application.streamingAssetsPath + $"/{bundleName}";
        }
    }
}