﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace AssetManagement
{
    public interface IAssetsLoader
    {
        UniTask<T> Load<T>(string path) where T : Object;
        UniTask<T> LoadAssetFromBundle<T>(string bundleName, string path) where T : Object;
    }
}