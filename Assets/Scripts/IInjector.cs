using UnityEngine;
using Zenject;

public interface IInjector
{
    DiContainer CurrentContainer { get; }
    void Inject(GameObject gameObject);
    void Inject(object obj);
}