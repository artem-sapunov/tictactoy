namespace Constants
{
    public class SceneNames
    {
        public const string MenuSceneName = "MenuScene";
        public const string GameplayScene = "GameplayScene";
    }
}