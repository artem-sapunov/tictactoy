namespace Constants
{
    public class AssetsPath
    {
        public const string AssetsProviderPath = "TicTacToeAssetsProvider";
        public const string AssetsProviderBundlePath = "Assets/TicTacToeAssetsProvider.asset";
        public const string CellPath = "Cell";
    }
}